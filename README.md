# my-own-touchbar

This is a really simple extension to add some nice features to my touchbar when i'm using vs-code.

## Features

For now, there are 3 buttons :

- `goToDeclaration`
- `toggleTerminal`
- `moveEditorToRightGroup`

## How to install

You need to copy the project to `~/.vscode/extensions/`.

To do that, you can run this command from the extensions's root folder : `cd .. && cp -r my-own-touchbar ~/.vscode/extensions`

**Enjoy!**
